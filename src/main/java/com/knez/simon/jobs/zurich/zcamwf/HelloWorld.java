package com.knez.simon.jobs.zurich.zcamwf;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class HelloWorld implements Serializable {

	private static final long serialVersionUID = -5418859642752468835L;
	
	public String provideMessage() {
		return "Hello World from Fuertefentura";
	}
	

}