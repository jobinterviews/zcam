package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import com.knez.simon.jobs.zurich.zcamwf.clients.EatingActivityClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;

@Named
@ViewScoped
public class EatingActivityBean extends ZcamBean {

	private static final long serialVersionUID = 8182476141336926197L;
	private static final Logger LOGGER = Logger.getLogger(EatingActivityBean.class.getSimpleName());
	
	@Inject
	private UserBean userBean;
	@Inject
	private EatingActivityClient eatingActivityClient;
	@Inject
	private EatingActivityEditBean eatingActivityEditBean;

	private List<EatingActivityDto> eatingActivites;
	
	@PostConstruct
	public void init() {
		if (userBean.isUserLoggedIn()) {
			refresh();
		} else {
			try {
				redirectToHome();
			} catch (IOException e) {
				LOGGER.warn("Could not redirect to home !");
			}
		}
	}
	
	public void refresh() {
		eatingActivites = eatingActivityClient.findAll(userBean.getCurrentUser());
	}
	
	public List<EatingActivityDto> getEatingActivites() {
		return eatingActivites;
	}
	
	public void selectEatingActivity(EatingActivityDto eatingActivity) {
		eatingActivityEditBean.configureEditBean(true, eatingActivity);
	}
	
	public void createEatingActivity() {
		EatingActivityDto eatingActivity = new EatingActivityDto();
		eatingActivity.setUser(userBean.getCurrentUser());
		eatingActivityEditBean.configureEditBean(false, eatingActivity);
	}
	
	public void deleteEatingActivity(EatingActivityDto eatingActivity) {
		eatingActivityClient.delete(eatingActivity.getId());
	}
	
	public void setEatingActivites(List<EatingActivityDto> eatingActivites) {
		this.eatingActivites = eatingActivites;
	}

	public EatingActivityEditBean getEatingActivityEditBean() {
		return eatingActivityEditBean;
	}

	public void setEatingActivityEditBean(EatingActivityEditBean eatingActivityEditBean) {
		this.eatingActivityEditBean = eatingActivityEditBean;
	}

}
