package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

import com.knez.simon.jobs.zurich.zcamwf.clients.EatingActivityClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;

@Named
@Dependent
public class EatingActivityEditBean extends ZcamBean {

	private static final long serialVersionUID = 8057448371345078170L;
	
	@Inject
	private EatingActivityClient eatingActivityClient;
	private boolean edit;
	private EatingActivityDto eatingActivity;
	
	private List<String> occasions;
	private List<String> foodCategories;
	private List<String> reasonsForEating;
	private List<String> companionships;
	
	@PostConstruct
	public void init() {
		occasions = Arrays.asList("Family visit", "Date", "Watching TV" ,"Picknik", "Party");
		foodCategories = Arrays.asList("Sweat snack", "Nuts", "Fast food", "Meat", "Fruit", "Salad", "Dairy");
		reasonsForEating = Arrays.asList("Hunger", "Boredom", "Physical Weakness" ,"Exploration");
		companionships = Arrays.asList("None", "Pet", "Family", "Friends", "Wife or partner", "Coworkers", "Business associates");
	}
	
	public void configureEditBean(boolean edit, EatingActivityDto eatingActivity) {
		this.edit = edit;
		setEatingActivity(eatingActivity);
	}
	
	public void save() {
		if (edit) {
			eatingActivityClient.replace(eatingActivity);
		} else {
			eatingActivityClient.create(eatingActivity);
		}
	}
	
	public EatingActivityDto getEatingActivity() {
		return eatingActivity;
	}

	public void setEatingActivity(EatingActivityDto eatingActivity) {
		this.eatingActivity = eatingActivity;
	}

	public List<String> getOccasions() {
		return occasions;
	}

	public void setOccasions(List<String> occasions) {
		this.occasions = occasions;
	}

	public EatingActivityClient getEatingActivityClient() {
		return eatingActivityClient;
	}

	public void setEatingActivityClient(EatingActivityClient eatingActivityClient) {
		this.eatingActivityClient = eatingActivityClient;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public List<String> getFoodCategories() {
		return foodCategories;
	}

	public void setFoodCategories(List<String> foodCategories) {
		this.foodCategories = foodCategories;
	}

	public List<String> getReasonsForEating() {
		return reasonsForEating;
	}

	public void setReasonsForEating(List<String> reasonsForEating) {
		this.reasonsForEating = reasonsForEating;
	}

	public List<String> getCompanionships() {
		return companionships;
	}

	public void setCompanionships(List<String> companionships) {
		this.companionships = companionships;
	}

}
