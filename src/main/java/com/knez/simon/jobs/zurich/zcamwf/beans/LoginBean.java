package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import net.bootsfaces.utils.FacesMessages;

@Named
@RequestScoped
public class LoginBean extends ZcamBean {

	private static final long serialVersionUID = 3645186227010680672L;
	@Inject
	private UserBean userBean;
	@NotNull(message="Username is required")
	private String username;
	@NotNull(message="Password is required")
	private String password;
	

	public void login() throws IOException {
		userBean.login(username, password);
		if (!userBean.isUserLoggedIn()) {
			FacesMessages.error("Error!", "Could not login");
			redirectToHome();
			return;
		} redirectToHomePrivate();
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
