package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.io.IOException;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;

import net.bootsfaces.utils.FacesMessages;

@Named
@RequestScoped
public class RegisterBean extends ZcamBean {

	private static final long serialVersionUID = -6186877769795516121L;
	@Inject
	private UserBean userBean;
	@NotNull
	@Size(min=4, message="Username is to short")
	private String username;
	@NotNull
	@Size(min=4, message="Password is to short")
	private String password;
	@NotNull
	private String passwordConfirm;
	

	public void register() throws IOException {
		Optional<UserDto> registeredUser = Optional.empty();
		if (password.equals(passwordConfirm)) {
			registeredUser = userBean.registerUser(username, password);
		} else {
			FacesMessages.error("Error!", "Password confirmation !");
		}
		if (!registeredUser.isPresent()) {
			FacesMessages.error("Error!", "Could not register!");
		} else {
			redirectToHome();
		}
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

}
