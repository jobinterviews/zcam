package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.io.IOException;
import java.util.Optional;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.knez.simon.jobs.zurich.zcamwf.clients.UserClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;

@Named
@SessionScoped
public class UserBean extends ZcamBean {

	private static final long serialVersionUID = -1996676751100731633L;
	
	@Inject 
	private UserClient userClient;
	
	private Optional<UserDto> loggedUser = Optional.empty();

	public boolean isUserLoggedIn() {
		return loggedUser.isPresent();
	}
	
	public void logout() throws IOException {
		loggedUser = Optional.empty();
		redirectToHome();
	}
	
	public void login(String username, String password) {
		loggedUser = userClient.login(username, password);
	}
	
	@Produces
	public UserDto getCurrentUser() {
		return loggedUser.get();
	}

	public Optional<UserDto> registerUser(String username, String password) {
		return userClient.registerUser(username, password);
	}
}
