package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.knez.simon.jobs.zurich.zcamwf.clients.UserClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;

@Named
@RequestScoped
public class UserSettingsBean extends ZcamBean {

	private static final long serialVersionUID = -2028689831847717456L;
	private UserDto user;
	@Inject
	private UserBean userBean;
	@Inject
	private UserClient userClient;
	private List<String> genders;
	private List<String> countries;

	@PostConstruct
	public void init() {
		genders = Arrays.asList("Male", "Female");
		countries = Arrays.asList("Slovenia", "Germany");
		if (userBean.isUserLoggedIn()) {
			user = userBean.getCurrentUser();
		}
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public void save() {
		userClient.replace(user);
	}

	public List<String> getGenders() {
		return genders;
	}

	public void setGenders(List<String> genders) {
		this.genders = genders;
	}

	public List<String> getCountries() {
		return countries;
	}

	public void setCountries(List<String> countries) {
		this.countries = countries;
	}

}
