package com.knez.simon.jobs.zurich.zcamwf.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.FacesContext;

import com.knez.simon.jobs.zurich.zcamwf.common.Constants;

/**
 * This is the main bean that all backing beans should extend from.
 * It holds common methods, such as redirections, updates, etc.
 * @author User
 *
 */
public abstract class ZcamBean implements Serializable {
	
	private static final long serialVersionUID = 4676244660748521419L;

	protected void redirectToHome() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.ZCAM_HOME_PAGE_PUBLIC.getValue());
	}
	
	protected void redirectToHomePrivate() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.ZCAM_HOME_PAGE_PRIVATE.getValue());
	}

}
