package com.knez.simon.jobs.zurich.zcamwf.beans.charts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.time.DateUtils;

import com.knez.simon.jobs.zurich.zcamwf.beans.UserBean;
import com.knez.simon.jobs.zurich.zcamwf.beans.ZcamBean;
import com.knez.simon.jobs.zurich.zcamwf.beans.charts.items.MealFrequencyChartItem;
import com.knez.simon.jobs.zurich.zcamwf.clients.EatingActivityClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;

@Named
@ViewScoped
public class MealFrequencyChartBean extends ZcamBean {

	private static final long serialVersionUID = 2043473791813358678L;
	private static Logger LOGGER = Logger.getLogger(MealFrequencyChartBean.class.getSimpleName());
	@Inject
	private UserBean userBean;
	@Inject
	private EatingActivityClient eatingActivityClient;
	private Map<Date, Integer> items;
	private List<MealFrequencyChartItem> chartItems;

	@PostConstruct
	public void init() {
		if (userBean.isUserLoggedIn()) { 
			populateItems();
		} else {
			try {
				redirectToHome();
			} catch (IOException e) {
				LOGGER.warning("Could not redirecet to home url!");
			}
		}
	}

	private void populateItems() {
		items = new HashMap<>();
		chartItems = new ArrayList<>();
		List<EatingActivityDto> eatingActivityList = eatingActivityClient.findAll(userBean.getCurrentUser());
		eatingActivityList.stream().forEach(this::createItemsEntry);
		items.forEach(this::pupulateChartItems);
	}

	private void createItemsEntry(EatingActivityDto eatingActivity) {
		Date date = eatingActivity.getTime();
		Date key;
		if (date != null) {
			key = DateUtils.truncate(date, java.util.Calendar.DAY_OF_MONTH);
			items.putIfAbsent(key, Integer.valueOf(0));
			int currentValue = items.get(key);
			items.put(key, currentValue + 1);
		}
	}
	
	private void pupulateChartItems(Date key, Integer sumValue) {
		chartItems.add(new MealFrequencyChartItem(key, sumValue));
		chartItems.sort(Comparator.comparing(MealFrequencyChartItem::getDate));
	}

	public List<MealFrequencyChartItem> getChartItems() {
		return chartItems;
	}

	public void setChartItems(List<MealFrequencyChartItem> chartItems) {
		this.chartItems = chartItems;
	}

	
}
