package com.knez.simon.jobs.zurich.zcamwf.beans.charts.external;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.knez.simon.jobs.zurich.zcamwf.beans.ZcamBean;
import com.knez.simon.jobs.zurich.zcamwf.beans.charts.items.SatisfactionByAgeChartItem;
import com.knez.simon.jobs.zurich.zcamwf.clients.EatingActivityClient;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;

@Named
@ViewScoped
public class AgeGroupsSatisfactionChartsBean extends ZcamBean {

	private static final long serialVersionUID = 8358116828719797760L;
	private static Logger LOGGER = Logger.getLogger(AgeGroupsSatisfactionChartsBean.class.getSimpleName());
	@Inject
	private EatingActivityClient eatingActivityClient;
	private Map<String, List<Integer>> items;
	private List<SatisfactionByAgeChartItem> chartItems;

	@PostConstruct
	public void init() {
		populateItems();
	}

	private void populateItems() {
		items = new HashMap<>();
		chartItems = new ArrayList<>();
		List<EatingActivityDto> eatingActivityList = eatingActivityClient.findAll();
		eatingActivityList.stream().forEach(this::createItemsEntry);
		items.forEach(this::pupulateChartItems);
	}

	private void createItemsEntry(EatingActivityDto eatingActivity) {
		if (eatingActivity.getUser() != null && eatingActivity.getUser().getBirthDate() != null) {
			String key;
			key = provideAgeCategory(eatingActivity.getUser().getBirthDate());
			items.putIfAbsent(key, new ArrayList<>());
			items.get(key).add(eatingActivity.getSatisfaction());
		}
	}

	private String provideAgeCategory(Date date) {
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int age = LocalDate.now().getYear() - localDate.getYear();
		if (age < 20) {
			return "Teens";
		} else if (age < 30) {
			return "20 yo";
		} else if (age < 40) {
			return "30 yo";
		} else if (age < 50) {
			return "40 yo";
		} else if (age < 60) {
			return "50 yo";
		} else
			return "Elderly";
	}

	private void pupulateChartItems(String key, List<Integer> entries) {
		chartItems.add(new SatisfactionByAgeChartItem(key, entries));
	}

	public List<SatisfactionByAgeChartItem> getChartItems() {
		return chartItems;
	}

	public void setChartItems(List<SatisfactionByAgeChartItem> chartItems) {
		this.chartItems = chartItems;
	}

}
