package com.knez.simon.jobs.zurich.zcamwf.beans.charts.items;

import java.util.Date;
import java.util.Objects;

public class MealFrequencyChartItem {

	private Date date;
	private Integer count;

	public MealFrequencyChartItem(Date date, Integer count) {
		this.date = date;
		this.count = count;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public int hashCode() {
		return Objects.hash(count, date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MealFrequencyChartItem other = (MealFrequencyChartItem) obj;
		return Objects.equals(count, other.count) && Objects.equals(date, other.date);
	}

	@Override
	public String toString() {
		return "MealFrequencyChartItem [date=" + date + ", count=" + count + "]";
	}

}