package com.knez.simon.jobs.zurich.zcamwf.beans.charts.items;

import java.util.List;
import java.util.Objects;

public class SatisfactionByAgeChartItem {
	private String ageRange;
	private double average;
	
	public SatisfactionByAgeChartItem(String ageRange, List<Integer> entries) {
		this.ageRange = ageRange;
		this.average = entries.stream().mapToInt(Integer::intValue).average().orElse(0);
	}
	public String getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	@Override
	public int hashCode() {
		return Objects.hash(ageRange, average);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SatisfactionByAgeChartItem other = (SatisfactionByAgeChartItem) obj;
		return Objects.equals(ageRange, other.ageRange)
				&& Double.doubleToLongBits(average) == Double.doubleToLongBits(other.average);
	}
	@Override
	public String toString() {
		return "SatisfactionByAgeChartItem [ageRange=" + ageRange + ", average=" + average + "]";
	}
	
	
}
