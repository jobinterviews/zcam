package com.knez.simon.jobs.zurich.zcamwf.clients;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.bson.types.ObjectId;

import com.knez.simon.jobs.zurich.zcamwf.common.PojoToMongoCollectionMapper;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;
import com.knez.simon.jobs.zurich.zcamwf.model.entity.EatingActivity;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.InsertOneResult;

@ApplicationScoped
public class EatingActivityClient extends ZcamClient {

	private MongoCollection<EatingActivity> collection;

	@PostConstruct
	public void init() {
		String collectionName = PojoToMongoCollectionMapper.provideCollectionName(EatingActivity.class.getSimpleName());
		collection = getDatabase().getCollection(collectionName, EatingActivity.class);
	}

	public String create(EatingActivityDto eatingActivity) {
		InsertOneResult insertOne = collection.insertOne(new EatingActivity(eatingActivity));
		return insertOne.getInsertedId().toString();
	}
	
	public void replace(EatingActivityDto eatingActivity) {
		BasicDBObject searchQuery = provideSearchQueryById(eatingActivity.getId());
		collection.replaceOne(searchQuery, new EatingActivity(eatingActivity));
	}
	
	public void delete(String id) {
		BasicDBObject searchQuery = provideSearchQueryById(id);
		collection.findOneAndDelete(searchQuery);
	}
	
	public Optional<EatingActivity> find(String id) {
		BasicDBObject searchQuery = provideSearchQueryById(id);
		FindIterable<EatingActivity> find = collection.find(searchQuery);
		return Optional.ofNullable(find.first());
		
	}

	private BasicDBObject provideSearchQueryById(String id) {
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("_id", new ObjectId(id));
		return searchQuery;
	}
	
	public List<EatingActivityDto> findAll() {
		BasicDBObject searchQuery = new BasicDBObject();
		FindIterable<EatingActivity> find = collection.find(searchQuery);
		return populateAndReturnResultList(find);
	}
	
	public List<EatingActivityDto> findAll(UserDto user) {
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("user.username", user.getUsername());
		FindIterable<EatingActivity> find = collection.find(searchQuery);
		return populateAndReturnResultList(find);
	}

	private List<EatingActivityDto> populateAndReturnResultList(FindIterable<EatingActivity> find) {
		List<EatingActivityDto> result = new ArrayList<>();
		for(EatingActivity eActivity : find) {
			result.add(eActivity.createDto());
		}
		return result;
	}
	
	
}
