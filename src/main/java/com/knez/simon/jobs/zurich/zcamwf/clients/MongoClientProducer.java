package com.knez.simon.jobs.zurich.zcamwf.clients;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.bson.codecs.configuration.CodecRegistry; 
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@ApplicationScoped
public class MongoClientProducer {
	@Produces
	public MongoClient create() {
		CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		ConnectionString connectionString = new ConnectionString("mongodb+srv://zcam_user:y.c.a.m.USer.751@clusterjobs.jxdly.mongodb.net/zcam?retryWrites=true&w=majority");
		MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
					.codecRegistry(pojoCodecRegistry)
					.applyConnectionString(connectionString)
					.build();
		return MongoClients.create(mongoClientSettings);
	}
	
	
}
