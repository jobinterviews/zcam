package com.knez.simon.jobs.zurich.zcamwf.clients;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.bson.types.ObjectId;

import com.knez.simon.jobs.zurich.zcamwf.common.PojoToMongoCollectionMapper;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;
import com.knez.simon.jobs.zurich.zcamwf.model.entity.User;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;

@ApplicationScoped
public class UserClient extends ZcamClient {

	private MongoCollection<User> collection;

	@PostConstruct
	public void init() {
		String collectionName = PojoToMongoCollectionMapper.provideCollectionName(User.class.getSimpleName());
		collection = getDatabase().getCollection(collectionName, User.class);
	}

	public String create(User user) {
		InsertOneResult insertOne = collection.insertOne(user);
		return insertOne.getInsertedId().toString();
	}
	
	public void replace(UserDto user) {
		BasicDBObject searchQuery = provideSearchQueryById(user.getId());
		FindIterable<User> find = collection.find(searchQuery);
		User currentUserEntity = find.first();
		User newUserEntity = new User(user);
		newUserEntity.setPassword(currentUserEntity.getPassword());
		collection.replaceOne(searchQuery, newUserEntity);
	}

	private BasicDBObject provideSearchQueryById(String id) {
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("_id", new ObjectId(id));
		return searchQuery;
	}

	public Optional<UserDto> login(String username, String password) {
		Optional<UserDto> response = Optional.empty();
		if (!username.isBlank() && !password.isBlank()) {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("username", username);
			FindIterable<User> find = collection.find(searchQuery);
			User first = find.first();
			if (first != null && first.getPassword().equals(password)) {
				return Optional.of(find.first().createDto());
			}
		}
		return response;
	}

	public Optional<UserDto> registerUser(String username, String password) {
		Optional<UserDto> response = Optional.empty();
		if (!username.isBlank() && !password.isBlank()) {
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("username", username);
			FindIterable<User> find = collection.find(searchQuery);
			if (find.first() == null) {
				response = registerNewUser(username, password);
			}
		} return response;

	}

	private Optional<UserDto> registerNewUser(String username, String password) {
		Optional<UserDto> response;
		User user = new User(username, password);
		create(user);
		UserDto userDto = new UserDto(user);
		response = Optional.of(userDto);
		return response;
	}
}
