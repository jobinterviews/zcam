package com.knez.simon.jobs.zurich.zcamwf.clients;

import javax.inject.Inject;

import com.knez.simon.jobs.zurich.zcamwf.common.Constants;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

public class ZcamClient {
	
	@Inject
	private MongoClient mongoClient;
	
	public MongoDatabase getDatabase() {
		return mongoClient.getDatabase(Constants.ZCAM_DB.getValue());
	}
}
