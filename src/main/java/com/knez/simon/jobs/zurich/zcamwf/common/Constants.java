package com.knez.simon.jobs.zurich.zcamwf.common;

public enum Constants {
	ZCAM_DB("zcam"),
	EMPTY_STRING(""),
	ZCAM_HOME_PAGE_PUBLIC("/zcamwf/statistics/public/ageSatisfaction.xhtml"),
	ZCAM_HOME_PAGE_PRIVATE("/zcamwf/eating/activity.xhtml");
	
	private String value;
	private Constants(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
