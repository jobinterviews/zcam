package com.knez.simon.jobs.zurich.zcamwf.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

public class DateTimeUtil {
	private DateTimeUtil() {

	}
	
	private static final Logger LOGGER = Logger.getLogger(DateTimeUtil.class.getSimpleName());
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	public static Date parseFromDateString(String dateString) {
		return parseStringToDate(dateString, DATE_FORMAT);
	}

	public static Date parseFromDateTimeString(String dateString){
		return parseStringToDate(dateString, DATE_TIME_FORMAT);
	}
	
	public static Date parseStringToDate(String dateString, String format){
		Date parsedDate = null;
		if (!StringUtils.isBlank(dateString)) {
			try {
				DateFormat formatter = new SimpleDateFormat(format);
				parsedDate = formatter.parse(dateString);
			} catch (Exception e) {
				LOGGER.warning("parseStringToDate(); Could not parse to date: " + dateString);
			}
		} return parsedDate;
	}

	public static String convertToString(Date date) {
		if (date == null) {
			return Constants.EMPTY_STRING.getValue();
		} 
		DateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);  
		return dateFormat.format(date); 
	}

}
