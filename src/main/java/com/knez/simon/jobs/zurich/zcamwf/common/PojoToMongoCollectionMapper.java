package com.knez.simon.jobs.zurich.zcamwf.common;

public class PojoToMongoCollectionMapper {
	private PojoToMongoCollectionMapper() {
		
	}
	
	public static String provideCollectionName(String className) {
		switch (className) {
			case "User": return "users";
			case "EatingActivity": return "eating_activities";
		} throw new IllegalArgumentException("Classname: " + className + " is not supported!");
	}
}
