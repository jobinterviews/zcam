package com.knez.simon.jobs.zurich.zcamwf.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class EatingActivityDto implements Serializable {

	private static final long serialVersionUID = -3249697267736361175L;

	private String id;
	private UserDto user;
	private Date time;
	private String foodCategory;
	private Integer satisfaction = 0;
	private String companionship;
	private String occasion;
	private String reason;

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getFoodCategory() {
		return foodCategory;
	}

	public void setFoodCategory(String foodCategory) {
		this.foodCategory = foodCategory;
	}

	public Integer getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(Integer satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getCompanionship() {
		return companionship;
	}

	public void setCompanionship(String companionship) {
		this.companionship = companionship;
	}

	public String getOccasion() {
		return occasion;
	}

	public void setOccasion(String occasion) {
		this.occasion = occasion;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(companionship, foodCategory, occasion, reason, satisfaction, time, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EatingActivityDto other = (EatingActivityDto) obj;
		return Objects.equals(companionship, other.companionship) && Objects.equals(foodCategory, other.foodCategory)
				&& Objects.equals(occasion, other.occasion) && Objects.equals(reason, other.reason)
				&& Objects.equals(satisfaction, other.satisfaction) && Objects.equals(time, other.time)
				&& Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		return "EeatingActivityDto [user=" + user + ", time=" + time + ", foodCategory=" + foodCategory
				+ ", satisfaction=" + satisfaction + ", companionship=" + companionship + ", occasion=" + occasion
				+ ", reason=" + reason + "]";
	}

}
