package com.knez.simon.jobs.zurich.zcamwf.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.knez.simon.jobs.zurich.zcamwf.common.DateTimeUtil;
import com.knez.simon.jobs.zurich.zcamwf.model.entity.User;

public class UserDto implements Serializable {

	private static final long serialVersionUID = 7158158968154781033L;

	private String id;
	private String username;
	private String gender;
	private String country;
	private Date birthDate;

	public UserDto() {

	}

	public UserDto(User user) {
		this.id = user.getId().toHexString();
		this.username = user.getUsername();
		this.gender = user.getGender();
		this.country = user.getCountry();
		this.birthDate = DateTimeUtil.parseFromDateTimeString(user.getBirthDate());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(birthDate, country, gender, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDto other = (UserDto) obj;
		return Objects.equals(birthDate, other.birthDate) && Objects.equals(country, other.country)
				&& Objects.equals(gender, other.gender) && Objects.equals(username, other.username);
	}

	@Override
	public String toString() {
		return "UserDto [username=" + username + ", gender=" + gender + ", country=" + country + ", birthDate="
				+ birthDate + "]";
	}

}
