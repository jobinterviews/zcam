package com.knez.simon.jobs.zurich.zcamwf.model.entity;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import org.codehaus.plexus.util.StringUtils;

import com.knez.simon.jobs.zurich.zcamwf.common.DateTimeUtil;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.EatingActivityDto;

import jakarta.nosql.mapping.Entity;

@Entity
public class EatingActivity {
	
	@BsonProperty("_id")
	@BsonId
	private ObjectId id;
	@NotNull
	private User user;
	private String dateTime;
	private String foodCategory;
	private String satisfaction;
	private String companionship;
	private String occasion;
	private String reason;
	
	public EatingActivity() {
		
	}
	
	public EatingActivity(EatingActivityDto dto) {
		if (dto.getId() != null) {
			this.id = new ObjectId(dto.getId());
		}
		this.user = new User(dto.getUser());
		this.companionship = dto.getCompanionship();
		this.foodCategory = dto.getFoodCategory();
		this.occasion = dto.getOccasion();
		this.reason = dto.getReason();
		if (dto.getSatisfaction() != null) {
			this.satisfaction = String.valueOf(dto.getSatisfaction());
		}
		this.dateTime = DateTimeUtil.convertToString(dto.getTime());
	}
	
	public EatingActivityDto createDto() {
		EatingActivityDto dto = new EatingActivityDto();
		dto.setId(id.toHexString());
		if (user != null) {
			dto.setUser(user.createDto());
		}
		dto.setTime(DateTimeUtil.parseFromDateTimeString(dateTime));
		dto.setFoodCategory(foodCategory);
		dto.setCompanionship(companionship);
		dto.setOccasion(occasion);
		dto.setReason(reason);
		if (StringUtils.isNotBlank(satisfaction)) {
			dto.setSatisfaction(Integer.valueOf(satisfaction));
		} else {
			dto.setSatisfaction(0);
		}
		return dto;
	}
	
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getFoodCategory() {
		return foodCategory;
	}

	public void setFoodCategory(String foodCategory) {
		this.foodCategory = foodCategory;
	}

	public String getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getCompanionship() {
		return companionship;
	}

	public void setCompanionship(String companionship) {
		this.companionship = companionship;
	}

	public String getOccasion() {
		return occasion;
	}

	public void setOccasion(String occasion) {
		this.occasion = occasion;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public int hashCode() {
		return Objects.hash(companionship, foodCategory, id, occasion, reason, satisfaction, dateTime, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EatingActivity other = (EatingActivity) obj;
		return Objects.equals(companionship, other.companionship) && Objects.equals(foodCategory, other.foodCategory)
				&& Objects.equals(id, other.id) && Objects.equals(occasion, other.occasion)
				&& Objects.equals(reason, other.reason) && Objects.equals(satisfaction, other.satisfaction)
				&& Objects.equals(dateTime, other.dateTime) && Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		return "EatingActivity [id=" + id + ", user=" + user + ", time=" + dateTime + ", foodCategory=" + foodCategory
				+ ", satisfaction=" + satisfaction + ", companionship=" + companionship + ", occasion=" + occasion
				+ ", reason=" + reason + "]";
	}

}
