package com.knez.simon.jobs.zurich.zcamwf.model.entity;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import com.knez.simon.jobs.zurich.zcamwf.common.DateTimeUtil;
import com.knez.simon.jobs.zurich.zcamwf.model.dto.UserDto;

import jakarta.nosql.mapping.Entity;

@Entity
public class User {

	@BsonProperty("_id")
	@BsonId
	private ObjectId id;
	@NotNull
	private String username;
	@NotNull
	private String password;
	private String gender;
	private String country;
	private String birthDate;
	
	
	public User() {}
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public UserDto createDto() {
		return new UserDto(this);
	}
	
	public User(UserDto userDto) {
		this.id = new ObjectId(userDto.getId());
		this.username = userDto.getUsername();
		this.gender = userDto.getGender();
		this.country = userDto.getCountry();
		this.birthDate = DateTimeUtil.convertToString(userDto.getBirthDate());
	}
	
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(birthDate, country, gender, id, password, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(birthDate, other.birthDate) && Objects.equals(country, other.country)
				&& Objects.equals(gender, other.gender) && Objects.equals(id, other.id)
				&& Objects.equals(password, other.password) && Objects.equals(username, other.username);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", gender=" + gender
				+ ", country=" + country + ", birthDate=" + birthDate + "]";
	}

}
